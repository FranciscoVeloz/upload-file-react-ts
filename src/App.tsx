import { ChangeEvent, FormEvent, useState } from "react";

interface form {
  txt1: string;
  txt2: string;
  txt3: File;
}

const initialState: form = {
  txt1: "",
  txt2: "",
  txt3: {
    lastModified: 0,
    name: "",
    webkitRelativePath: "",
    size: 0,
    type: "",
    arrayBuffer: function (): Promise<ArrayBuffer> {
      throw new Error("Function not implemented.");
    },
    slice: function (
      start?: number | undefined,
      end?: number | undefined,
      contentType?: string | undefined
    ): Blob {
      throw new Error("Function not implemented.");
    },
    stream: function (): ReadableStream<Uint8Array> {
      throw new Error("Function not implemented.");
    },
    text: function (): Promise<string> {
      throw new Error("Function not implemented.");
    },
  },
};

const App = () => {
  const [data, setData] = useState<form>(initialState);
  const [image, setImage] = useState<string>("");

  const handleChange = (e: ChangeEvent<HTMLInputElement>) => {
    if (e.target.files) {
      setImage(URL.createObjectURL(e.target.files[0]));
      return setData({ ...data, [e.target.name]: e.target.files[0] });
    }

    setData({ ...data, [e.target.name]: e.target.value });
  };

  const handleSubmit = (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault();

    console.log(data);
    const formData = new FormData();
    formData.append("txt1", data.txt1);
    formData.append("txt2", data.txt2);
    formData.append("txt3", data.txt3, data.txt3.name);

    console.log(formData);
  };

  return (
    <div className="container">
      <h1 className="text-center">Upload file</h1>

      <form className="form-container" onSubmit={handleSubmit}>
        <input
          type="text"
          name="txt1"
          placeholder="Text 1"
          className="input"
          onChange={handleChange}
        />

        <input
          type="text"
          name="txt2"
          placeholder="Text 2"
          className="input"
          onChange={handleChange}
        />

        <input
          type="file"
          name="txt3"
          className="input"
          onChange={handleChange}
        />

        <button className="btn">Send</button>
      </form>

      <img src={image} alt={data.txt3.name} className="image" />
    </div>
  );
};

export default App;
